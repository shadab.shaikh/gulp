let gulp = require('gulp');
let sass = require('gulp-sass');
sass.compiler = require('node-sass');

let sourcemaps = require('gulp-sourcemaps');
let minify = require('gulp-minify');
let concatJs = require('gulp-concat');
let uglifyCss = require('gulp-clean-css');
const { watch } = require('gulp');


gulp.task('welcome-msg',async function(){
    return console.log(`welcome to gulp...`);
});


gulp.task('copy-files',function(){
    return gulp.src('src/css/*.css')
               .pipe(gulp.dest('build/css'));
});

gulp.task('sass',function(){
    return gulp.src('src/scss/*.scss')
               .pipe(sass().on('error',sass.logError))
               .pipe(concatJs('style.css'))
               .pipe(uglifyCss())
               .pipe(gulp.dest('build/css/'))
});


gulp.task('concat',function(){
    return gulp.src('src/js/*.js')
               .pipe(concatJs('sources.js'))
               .pipe(gulp.dest('src/concatjs/'))
});

gulp.task('js-minify',function(){
    return gulp.src('src/concatjs/sources.js')
               .pipe(sourcemaps.init())
               .pipe(minify({
                    ext:{
                    min:'.min.js'
                    },
                    noSource: true,
                    ignoreFiles: []
                }))
               .pipe(sourcemaps.write('.'))
               .pipe(gulp.dest('build/js/'));
});


gulp.task('watch',function(){
    gulp.watch('src/js/*.js',gulp.series('concat','js-minify','sass'));
})

gulp.task('default', gulp.series('watch'));